#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAME_LENGTH 50
#define DATE_LENGTH 11 // For YYYY-MM-DD + null terminator
#define REGISTRATION_LENGTH 7 // 6 digits + null terminator
#define PROGRAM_CODE_LENGTH 5 // 4 characters + null terminator
#define MAX_STUDENTS 100

typedef struct {
    char name[MAX_NAME_LENGTH];
    char dob[DATE_LENGTH];
    char registration[REGISTRATION_LENGTH];
    char program_code[PROGRAM_CODE_LENGTH];
    float tuition;
} Student;

Student students[MAX_STUDENTS];
int num_students = 0;

void display_menu() {
    printf("\nMenu:\n");
    printf("1. Add Student\n");
    printf("2. View All Students\n");
    printf("3. Search by Registration Number\n");
    printf("4. Sort Students\n");
    printf("5. Export to CSV\n");
    printf("6. Exit\n");
}

void add_student() {
    if (num_students >= MAX_STUDENTS) {
        printf("Maximum number of students reached.\n");
        return;
    }
    Student new_student;
    printf("Enter student name: ");
    scanf("%s", new_student.name);
    printf("Enter student date of birth (YYYY-MM-DD): ");
    scanf("%s", new_student.dob);
    printf("Enter registration number: ");
    scanf("%s", new_student.registration);
    printf("Enter program code: ");
    scanf("%s", new_student.program_code);
    printf("Enter annual tuition: ");
    scanf("%f", &new_student.tuition);
    
    students[num_students++] = new_student;
    printf("Student added successfully.\n");
}

void view_all_students() {
    printf("\nAll Students:\n");
    for (int i = 0; i < num_students; i++) {
        printf("Name: %s, DOB: %s, Reg No: %s, Program Code: %s, Tuition: %.2f\n",
               students[i].name, students[i].dob, students[i].registration, students[i].program_code, students[i].tuition);
    }
}

void search_by_registration() {
    char reg_no[REGISTRATION_LENGTH];
    printf("Enter registration number to search: ");
    scanf("%s", reg_no);
    for (int i = 0; i < num_students; i++) {
        if (strcmp(students[i].registration, reg_no) == 0) {
            printf("Student found:\n");
            printf("Name: %s, DOB: %s, Reg No: %s, Program Code: %s, Tuition: %.2f\n",
                   students[i].name, students[i].dob, students[i].registration, students[i].program_code, students[i].tuition);
            return;
        }
    }
    printf("Student with registration number %s not found.\n", reg_no);
}

void swap_students(Student *a, Student *b) {
    Student temp = *a;
    *a = *b;
    *b = temp;
}

void sort_students_by_name() {
    for (int i = 0; i < num_students - 1; i++) {
        for (int j = 0; j < num_students - i - 1; j++) {
            if (strcmp(students[j].name, students[j + 1].name) > 0) {
                swap_students(&students[j], &students[j + 1]);
            }
        }
    }
    printf("Students sorted by name.\n");
}

void sort_students_by_tuition() {
    for (int i = 0; i < num_students - 1; i++) {
        for (int j = 0; j < num_students - i - 1; j++) {
            if (students[j].tuition > students[j + 1].tuition) {
                swap_students(&students[j], &students[j + 1]);
            }
        }
    }
    printf("Students sorted by tuition.\n");
}

void export_to_csv() {
    FILE *fp = fopen("students.csv", "a");
    if (fp == NULL) {
        printf("Error opening file.\n");
        return;
    }
    for (int i = 0; i < num_students; i++) {
        fprintf(fp, "%s,%s,%s,%s,%.2f\n", students[i].name, students[i].dob, students[i].registration, students[i].program_code, students[i].tuition);
    }
    fclose(fp);
    printf("Students exported to CSV successfully.\n");
}

int main() {
    int choice;
    do {
        display_menu();
        printf("Enter your choice: ");
        scanf("%d", &choice);
        switch(choice) {
            case 1:
                add_student();
                break;
            case 2:
                view_all_students();
                break;
            case 3:
                search_by_registration();
                break;
            case 4:
                printf("Sort by:\n");
                printf("1. Name\n");
                printf("2. Tuition\n");
                printf("Enter your choice: ");
                scanf("%d", &choice);
                if (choice == 1)
                    sort_students_by_name();
                else if (choice == 2)
                    sort_students_by_tuition();
                else
                    printf("Invalid choice.\n");
                break;
            case 5:
                export_to_csv();
                break;
            case 6:
                printf("Exiting program.\n");
                break;
            default:
                printf("Invalid choice.\n");
        }
    } while (choice != 6);
    return 0;
}
