void view_all_students() {
    printf("\nAll Students:\n");
    for (int i = 0; i < num_students; i++) {
        printf("Name: %s, DOB: %s, Reg No: %s, Program Code: %s, Tuition: %.2f\n",
               students[i].name, students[i].dob, students[i].registration, students[i].program_code, students[i].tuition);
    }
}

void search_by_registration() {
    char reg_no[REGISTRATION_LENGTH];
    printf("Enter registration number to search: ");
    scanf("%s", reg_no);
    for (int i = 0; i < num_students; i++) {
        if (strcmp(students[i].registration, reg_no) == 0) {
            printf("Student found:\n");
            printf("Name: %s, DOB: %s, Reg No: %s, Program Code: %s, Tuition: %.2f\n",
                   students[i].name, students[i].dob, students[i].registration, students[i].program_code, students[i].tuition);
            return;
        }
    }
    printf("Student with registration number %s not found.\n", reg_no);
}
