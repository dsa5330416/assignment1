# Assignment1

The data structure used in this code is an array of structures (Student students[MAX_STUDENTS]) to store information about multiple students. Each student's data is represented by a struct Student, which contains fields for their name, date of birth, registration number, program code, and tuition fee.

Here are some considerations for selecting this data structure:

Efficiency: Arrays provide direct access to elements based on their indices, which makes accessing and modifying individual student records efficient. This is important for operations like adding, searching, sorting, and displaying students.

Simplicity: Arrays are a simple and straightforward data structure in C, making the code easier to understand and maintain, especially for beginners.

Static Size: Using an array allows for a fixed number of students (MAX_STUDENTS) to be stored. This limitation may be appropriate depending on the application's requirements and memory constraints.

Sequential Access: The array allows for sequential access to student records, which is suitable for tasks like displaying all students or exporting them to a CSV file.

Search and Sort: Although searching and sorting operations are less efficient compared to other data structures like trees or hash tables, they are still feasible for the number of students considered in this program (up to 100 students).



Individual video explaining individual Part

1. Tusiime Emily Queen
23/U/26811/PS
https://youtu.be/L1mHGMUV9Yg

2. Ssimbwa  Bright  Samuel 
23/U/1437
https://youtu.be/D9oX903rhKU?si=2XOA7pQJ2myLSINK

3. Kisaka Maurice Baraka
23/u/24067/EVE
https://youtu.be/M5IK2TM06Gc?si=tT1Gi5AgzReTe2zM

4. Kayiwa Jashobeam Magezi 
23/U/0559
https://youtu.be/wriTXspeNxc?si=_PKFKUKZFANl9TjI

5.Mwesigwa Jordan
23/U/12543/PS
https://youtu.be/wk22BPS6yWI?si=Ce2cRqdUejE1Z0lP