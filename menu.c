// including necessary libraries

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

    // defining constants
#define MAX_NAME_LENGTH 50
#define DATE_LENGTH 11 // For YYYY-MM-DD + null terminator
#define REGISTRATION_LENGTH 7 // 6 digits + null terminator
#define PROGRAM_CODE_LENGTH 5 // 4 characters + null terminator
#define MAX_STUDENTS 100

  // defining structure of a student
typedef struct {
    char name[MAX_NAME_LENGTH];
    char dob[DATE_LENGTH];
    char registration[REGISTRATION_LENGTH];
    char program_code[PROGRAM_CODE_LENGTH];
    float tuition;
} Student;

Student students[MAX_STUDENTS];
int num_students = 0;

void display_menu() {
    printf("\nMenu:\n");
    printf("1. Add Student\n");
    printf("2. View All Students\n");
    printf("3. Search by Registration Number\n");
    printf("4. Sort Students by Name\n");
    printf("5. Sort Students by Tuition\n");
    printf("6. Exit\n");
}



void add_student() {
    if (num_students >= MAX_STUDENTS) {
        printf("Maximum number of students reached.\n");
        return;
    }
    Student new_student;
    printf("Enter student name: ");
    scanf("%s", new_student.name);
    printf("Enter student date of birth (YYYY-MM-DD): ");
    scanf("%s", new_student.dob);
    printf("Enter registration number: ");
    scanf("%s", new_student.registration);
    printf("Enter program code: ");
    scanf("%s", new_student.program_code);
    printf("Enter annual tuition: ");
    scanf("%f", &new_student.tuition);

    students[num_students++] = new_student;
    printf("Student added successfully.\n");
}
